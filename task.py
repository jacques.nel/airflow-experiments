import datetime as dt

from airflow import models
from kubernetes.client import models as k8s
# from airflow.providers.cncf.kubernetes.operators import kubernetes_pod_operator
from airflow.providers.cncf.kubernetes.operators import kubernetes_pod
from airflow.operators.bash_operator import BashOperator


with models.DAG(
        dag_id='kubernetes_test_dag',
        schedule_interval=dt.timedelta(days=1),
        start_date=dt.datetime.now() - dt.timedelta(days=1)) as dag:

    kub_min_pod = kubernetes_pod.KubernetesPodOperator(
        namespace='default',
        image='localhost:5000/migrations/ingestion-service:latest',
        image_pull_secrets=[k8s.V1LocalObjectReference('local-registry')],
        task_id='foo-task',
        name='foo-task',
        #        cmds=['cat'],
        #        arguments=['requirements.txt'],
        cmds=['python'],
        arguments=[
            'ingestion-service/main.py',
            '--vendor',
            'quandl',
            '--dataset',
            'sharadar_tickers_sep'],
        get_logs=True,
        dag=dag,
        env_vars={'ENVIRONMENT_TARGET': 'dev'},
        #        is_delete_operator_pod=True,
        in_cluster=True,
        image_pull_policy='Always')

#    BashOperator(
#        task_id='bash_foo',
#        bash_command='pwd',
#        dag=dag)


# from airflow import DAG
# from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
# from airflow.operators.dummy_operator import DummyOperator

# default_args = {
#    'owner': 'airflow',
#    'depends_on_past': False,
#    'start_date': dt.datetime.utcnow(),
#    'email': ['airflow@example.com'],
#    'email_on_failure': False,
#    'email_on_retry': False,
#    'retries': 1,
#    'retry_delay': dt.timedelta(minutes=5)
# }


# dag = DAG('kubernetes_example',
#          default_args=default_args,
#          schedule_interval=dt.timedelta(minutes=10))

# start = DummyOperator(task_id='run_this_first', dag=dag)

# passing = KubernetesPodOperator(
#    namespace='default',
#    image='Python:3.8.5',
#    cmds=['python', '-c'],
#    arguments=['print("hello world")'],
#    labels={'foo': 'bar'},
#    name='passing-test',
#    task_id='passing-task',
#    get_logs=True,
#    dag=dag)

# passing.set_upstream(start)
